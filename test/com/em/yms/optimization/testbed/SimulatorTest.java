package com.em.yms.optimization.testbed;

import static com.em.yms.optimization.testbed.Simulator.Algorithm.SCHEDULE_AND_ORDERLINE_ONLY;
import com.em.yms.optimization.testbed.Simulator.MultipleInSameOrderline;
import static com.em.yms.optimization.testbed.Simulator.SLAB_ID_STARTING_ID;
import com.em.yms.optimization.testbed.Simulator.SimulationResults;
import com.em.yms.optimization.testbed.algorithm.MaterialSelector;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Edward Ames
 */
public class SimulatorTest {

    private Map<Integer, Integer> schedules;
    private Map<Integer, MultipleInSameOrderline> orderlineGroups;

    @Before
    public void setUp() {
        schedules = new HashMap<>();
        schedules.put(425, 200);
        schedules.put(426, 200);
        schedules.put(427, 200);

        orderlineGroups = new HashMap<>();
        MultipleInSameOrderline orderline = new MultipleInSameOrderline();
        orderline.orderline = 20;
        orderline.numInOrderline = 10;
        orderlineGroups.put(426, orderline);
    }

    @Test
    public void testSetupSlabs() {
        // Setup
        Simulator instance = new Simulator();
        Inventory inventory = instance.getInventory();
        
        // Exercise
        instance.setupSlabs(schedules, orderlineGroups);

        // Verify
        assertEquals(600, inventory.getNumSlabs());
        int slabId = SLAB_ID_STARTING_ID;
        Slab slab = inventory.getSlab(slabId);
        assertEquals(425, slab.getScheduleNumber());
        assertEquals(1, slab.getOrderlineNumber());

        slabId = SLAB_ID_STARTING_ID + 199;
        slab = inventory.getSlab(slabId);
        assertEquals(425, slab.getScheduleNumber());
        assertEquals(200, slab.getOrderlineNumber());

        slabId = SLAB_ID_STARTING_ID + 200;
        slab = inventory.getSlab(slabId);
        assertEquals(426, slab.getScheduleNumber());
        assertEquals(1, slab.getOrderlineNumber());

        slabId = SLAB_ID_STARTING_ID + 229;
        slab = inventory.getSlab(slabId);
        assertEquals(426, slab.getScheduleNumber());
        assertEquals(20, slab.getOrderlineNumber());
    }

    @Test
    public void testRunSimOneRun() {
        // Setup
        Simulator instance = new Simulator();
        instance.setupSlabs(schedules, orderlineGroups);
        List<Integer> runSchedules = new ArrayList<>();
        runSchedules.add(425);
//        System.out.println(instance.getBinLayout());
        
        // Exercise
        Simulator.Algorithm algoType = SCHEDULE_AND_ORDERLINE_ONLY;
        SimulationResults actualResults = instance.runSim(1, algoType, runSchedules);

        // Verify
        assertTrue("Shouldn't be zero moves", actualResults.avgNumMoves > 0);
        assertTrue("Shouldn't be zero distance", actualResults.avgDist > 0);
        assertEquals("N = 1", 1, actualResults.n);
    }

    @Test
    public void testRunSimTenRuns() {
        // Setup
        Simulator instance = new Simulator();
        instance.setupSlabs(schedules, orderlineGroups);
        List<Integer> runSchedules = new ArrayList<>();
        runSchedules.add(425);
//        System.out.println(instance.getBinLayout());
        
        // Exercise
        Simulator.Algorithm algoType = SCHEDULE_AND_ORDERLINE_ONLY;
        SimulationResults actualResults = instance.runSim(10, algoType, runSchedules);

        // Verify
        assertTrue("Shouldn't be zero moves", actualResults.avgNumMoves > 0);
        assertTrue("Shouldn't be zero distance", actualResults.avgDist > 0);
        assertEquals("N = 10", 10, actualResults.n);
    }
}
