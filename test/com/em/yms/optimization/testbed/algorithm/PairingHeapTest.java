package com.em.yms.optimization.testbed.algorithm;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Edward Ames
 */
public class PairingHeapTest {

    /**  Tolerance for floating-point comparison.  */
    private final static double TOL = 0.01;

    @Test
    public void testFindMin() {
        // Setup
        PairingHeap<Integer> instance = new PairingHeap<>(4, new ArrayList<>());

        // Exercise
        int actualMin = instance.findMin();

        // Verify
        int expectedMin = 4;
        assertEquals(expectedMin, actualMin);

        // Exercise
        instance = new PairingHeap(-1, new ArrayList<>());
        actualMin = instance.findMin();

        // Verify
        expectedMin = -1;
        assertEquals(expectedMin, actualMin);
    }

    @Test
    public void testMerge() {
        // Setup
        PairingHeap<Integer> instance = new PairingHeap<>(4, new ArrayList<>());
        PairingHeap<Integer> second = new PairingHeap<>(5, new ArrayList<>());

        // Exercise
        PairingHeap actualHeap = instance.merge(second);

        // Verify
        int expectedMin = 4;
        assertEquals(expectedMin, actualHeap.findMin());

        // Exercise
        PairingHeap third = new PairingHeap<>(15, new ArrayList<>());
        actualHeap = actualHeap.merge(third);

        // Verify
        expectedMin = 4;
        assertEquals(expectedMin, actualHeap.findMin());

        // Exercise
        PairingHeap fourth = new PairingHeap<>(1, new ArrayList<>());
        actualHeap = actualHeap.merge(fourth);

        // Verify
        expectedMin = 1;
        assertEquals(expectedMin, actualHeap.findMin());

        // Exercise
        PairingHeap fifth = new PairingHeap<>(-1, new ArrayList<>());
        actualHeap = actualHeap.merge(fifth);

        // Verify
        expectedMin = -1;
        assertEquals(expectedMin, actualHeap.findMin());
    }

    @Test
    public void testInsert() {
        // Setup
        PairingHeap<Integer> instance = new PairingHeap<>(4, new ArrayList<>());

        // Exercise
        PairingHeap actualHeap = instance.insert(5);

        // Verify
        int expectedMin = 4;
        assertEquals(expectedMin, actualHeap.findMin());

        // Exercise
        actualHeap = instance.insert(15);

        // Verify
        expectedMin = 4;
        assertEquals(expectedMin, actualHeap.findMin());

        // Exercise
        actualHeap = instance.insert(1);

        // Verify
        expectedMin = 1;
        assertEquals(expectedMin, actualHeap.findMin());

        // Exercise
        actualHeap = instance.insert(-1);

        // Verify
        expectedMin = -1;
        assertEquals(expectedMin, actualHeap.findMin());
    }

    @Test
    public void testDeleteMin() {
        // Setup
        PairingHeap<Integer> instance = new PairingHeap<>(4, new ArrayList<>());
        PairingHeap a = instance.insert(5).insert(15).insert(25).insert(35);

        // Exercise
        PairingHeap actualHeap = a.deleteMin();

        // Verify
        int expectedMin = 5;
        assertEquals(expectedMin, actualHeap.findMin());

        // Exercise
        actualHeap = actualHeap.deleteMin();

        // Verify
        expectedMin = 15;
        assertEquals(expectedMin, actualHeap.findMin());

        // Exercise
        actualHeap = actualHeap.deleteMin();

        // Verify
        expectedMin = 25;
        assertEquals(expectedMin, actualHeap.findMin());

        // Exercise
        actualHeap = actualHeap.deleteMin();

        // Verify
        expectedMin = 35;
        assertEquals(expectedMin, actualHeap.findMin());
    }

    @Test
    public void testFindMinEdge() {
        // Setup
        Edge firstEdge = new Edge(1, 2, 4);
        PairingHeap<Edge> instance = new PairingHeap<>(firstEdge, new ArrayList<>());

        // Exercise
        Edge actualMin = instance.findMin();

        // Verify
        double expectedMin = 4;
        assertEquals(expectedMin, actualMin.weight(), TOL);

        // Exercise
        Edge secondEdge = new Edge(2, 3, 1.3);
        instance = new PairingHeap(secondEdge, new ArrayList<>());
        actualMin = instance.findMin();

        // Verify
        expectedMin = 1.3;
        assertEquals(expectedMin, actualMin.weight(), TOL);
    }

    @Test
    public void testMergeEdge() {
        // Setup
        Edge firstEdge = new Edge(1, 2, 4);
        Edge secondEdge = new Edge(2, 3, 5);
        Edge thirdEdge = new Edge(2, 4, 15);
        Edge fourthEdge = new Edge(4, 5, 1);
        Edge fifthEdge = new Edge(5, 6, 0.1);

        PairingHeap<Edge> instance = new PairingHeap<>(firstEdge, new ArrayList<>());
        PairingHeap<Edge> second = new PairingHeap<>(secondEdge, new ArrayList<>());

        // Exercise
        PairingHeap<Edge> actualHeap = instance.merge(second);

        // Verify
        int expectedMin = 4;
        assertEquals(expectedMin, actualHeap.findMin().weight(), TOL);

        // Exercise
        PairingHeap<Edge> third = new PairingHeap<>(thirdEdge, new ArrayList<>());
        actualHeap = actualHeap.merge(third);

        // Verify
        expectedMin = 4;
        assertEquals(expectedMin, actualHeap.findMin().weight(), TOL);

        // Exercise
        PairingHeap<Edge> fourth = new PairingHeap<>(fourthEdge, new ArrayList<>());
        actualHeap = actualHeap.merge(fourth);

        // Verify
        expectedMin = 1;
        assertEquals(expectedMin, actualHeap.findMin().weight(), TOL);

        // Exercise
        PairingHeap<Edge> fifth = new PairingHeap<>(fifthEdge, new ArrayList<>());
        actualHeap = actualHeap.merge(fifth);

        // Verify
        assertEquals(0.1, actualHeap.findMin().weight(), TOL);
    }

    @Test
    public void testInsertEdge() {
        // Setup
        Edge firstEdge = new Edge(1, 2, 4);
        Edge secondEdge = new Edge(2, 3, 5);
        Edge thirdEdge = new Edge(2, 4, 15);
        Edge fourthEdge = new Edge(4, 5, 1);
        Edge fifthEdge = new Edge(5, 6, 0.1);

        PairingHeap<Edge> instance = new PairingHeap<>(firstEdge, new ArrayList<>());

        // Exercise
        PairingHeap<Edge> actualHeap = instance.insert(secondEdge);

        // Verify
        int expectedMin = 4;
        assertEquals(expectedMin, actualHeap.findMin().weight(), TOL);

        // Exercise
        actualHeap = instance.insert(thirdEdge);

        // Verify
        expectedMin = 4;
        assertEquals(expectedMin, actualHeap.findMin().weight(), TOL);

        // Exercise
        actualHeap = instance.insert(fourthEdge);

        // Verify
        expectedMin = 1;
        assertEquals(expectedMin, actualHeap.findMin().weight(), TOL);

        // Exercise
        actualHeap = instance.insert(fifthEdge);

        // Verify
        assertEquals(0.1, actualHeap.findMin().weight(), TOL);
    }

    @Test
    public void testDeleteMinEdge() {
        // Setup
        Edge firstEdge = new Edge(1, 2, 4);
        Edge secondEdge = new Edge(2, 3, 5);
        Edge thirdEdge = new Edge(2, 4, 15);
        Edge fourthEdge = new Edge(4, 5, 25);
        Edge fifthEdge = new Edge(5, 6, 35);

        PairingHeap<Edge> instance = new PairingHeap<>(firstEdge, new ArrayList<>());
        PairingHeap<Edge> a = instance
                .insert(secondEdge)
                .insert(thirdEdge)
                .insert(fourthEdge)
                .insert(fifthEdge);

        // Exercise
        PairingHeap<Edge> actualHeap = a.deleteMin();

        // Verify
        int expectedMin = 5;
        assertEquals(expectedMin, actualHeap.findMin().weight(), TOL);

        // Exercise
        actualHeap = actualHeap.deleteMin();

        // Verify
        expectedMin = 15;
        assertEquals(expectedMin, actualHeap.findMin().weight(), TOL);

        // Exercise
        actualHeap = actualHeap.deleteMin();

        // Verify
        expectedMin = 25;
        assertEquals(expectedMin, actualHeap.findMin().weight(), TOL);

        // Exercise
        actualHeap = actualHeap.deleteMin();

        // Verify
        expectedMin = 35;
        assertEquals(expectedMin, actualHeap.findMin().weight(), TOL);
    }
}
