package com.em.yms.optimization.testbed.algorithm.simple;

import com.em.yms.optimization.testbed.BinLayout;
import com.em.yms.optimization.testbed.Inventory;
import com.em.yms.optimization.testbed.Simulator;
import com.em.yms.optimization.testbed.Slab;
import com.em.yms.optimization.testbed.algorithm.MaterialSelector;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author ed
 */
public class ScheduleAndOrderlineOnlyTest {
    private Inventory inventory;
    private BinLayout bins;

    @Before
    public void setup() {
        Map<Integer, Integer> schedules = new HashMap<>();
        schedules.put(425, 200);
        schedules.put(426, 200);
        schedules.put(427, 200);

        Map<Integer, Simulator.MultipleInSameOrderline> orderlineGroups = new HashMap<>();
        Simulator.MultipleInSameOrderline orderline = new Simulator.MultipleInSameOrderline();
        orderline.orderline = 20;
        orderline.numInOrderline = 10;
        orderlineGroups.put(426, orderline);

        Simulator simulator = new Simulator();
        simulator.setupSlabs(schedules, orderlineGroups);
        inventory = simulator.getInventory();
        bins = simulator.getBinLayout();
    }

    @Test
    public void testSort() {
        // Setup
        ScheduleAndOrderlineOnly instance = 
                new ScheduleAndOrderlineOnly(inventory, bins);
        List<Integer> schedules = new ArrayList<>();
        schedules.add(425);

        // Exercise
        List<Slab> actualSlabs = instance.prepareSlabs(schedules);

        // Verify
        assertEquals("Wrong number of scheduled slabs", 200, actualSlabs.size());
        int orderline = 0;
        for (Slab slab : actualSlabs) {
            assertTrue("Should be in increasing orderline sequence",
                    slab.getOrderlineNumber() > orderline);
            orderline = slab.getOrderlineNumber();
        }
    }

    @Test
    public void testSortMultipleSchedules() {
        // Setup
        ScheduleAndOrderlineOnly instance = 
                new ScheduleAndOrderlineOnly(inventory, bins);
        List<Integer> schedules = new ArrayList<>();
        schedules.add(425);
        schedules.add(426);

        // Exercise
        List<Slab> actualSlabs = instance.prepareSlabs(schedules);

        // Verify
        assertEquals("Wrong number of scheduled slabs", 400, actualSlabs.size());

        // Schedule 424
        int orderline = 0;
        for (int i = 0; i < 200; i++) {
            Slab slab = actualSlabs.get(i);
            assertTrue("Should be in increasing orderline sequence",
                    slab.getOrderlineNumber() > orderline);
            orderline = slab.getOrderlineNumber();
        }
        // Schedule 425 -- has some slabs with same orderline
        orderline = 0;
        for (int i = 200; i < 400; i++) {
            Slab slab = actualSlabs.get(i);
            assertTrue("Should be in increasing orderline sequence",
                    slab.getOrderlineNumber() >= orderline);
            orderline = slab.getOrderlineNumber();
        }
    }

    @Test
    public void testSimulateOneScheduleNum() {
        // Setup
        ScheduleAndOrderlineOnly instance = 
                new ScheduleAndOrderlineOnly(inventory, bins);
        List<Integer> schedules = new ArrayList<>();
        schedules.add(427);

        // Exercise
        MaterialSelector.RunData actualResults = instance.simulate(schedules);

        // Verify
        assertTrue("Shouldn't be zero distance", actualResults.distance > 0);
        int expectedNumMoves = 200 / 4;
        assertEquals("Wrong number of moves", expectedNumMoves, actualResults.moves.size());
        for (List<Integer> slabsInPlannedMove : actualResults.moves) {
            assertTrue("Number of slabs moved must be equal or less than 4.",
                    slabsInPlannedMove.size() <= 5);
            assertTrue("Number of slabs moved must greater than 0.",
                    slabsInPlannedMove.size() > 0);
        }
    }

    @Test
    public void testSimulateTwoScheduleNum() {
        // Setup
        ScheduleAndOrderlineOnly instance = 
                new ScheduleAndOrderlineOnly(inventory, bins);
        List<Integer> schedules = new ArrayList<>();
        schedules.add(425);
        schedules.add(426);

        // Exercise
        MaterialSelector.RunData actualResults = instance.simulate(schedules);

        // Verify
        assertTrue("Shouldn't be zero distance", actualResults.distance > 0);
        int expectedNumMoves = 400 / 4;
        assertEquals("Wrong number of moves", expectedNumMoves, actualResults.moves.size());
        for (List<Integer> slabsInPlannedMove : actualResults.moves) {
            assertTrue("Number of slabs moved must be equal or less than 4.",
                    slabsInPlannedMove.size() <= 5);
            assertTrue("Number of slabs moved must greater than 0.",
                    slabsInPlannedMove.size() > 0);
        }
    }
}
