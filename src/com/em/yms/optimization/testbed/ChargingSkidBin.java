package com.em.yms.optimization.testbed;

import com.em.yms.optimization.testbed.Bin;

/**
 *
 * @author Edward Ames
 */
public class ChargingSkidBin extends Bin {
    ChargingSkidBin(int binId, int xx, int yy, int n) {
        super(binId, xx, yy, n);
    }
}
