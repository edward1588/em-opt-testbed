package com.em.yms.optimization.testbed;

/**
 *
 * @author Edward Ames
 */
public class Slab {
    private final int slabId;
    private final int scheduleNumber;
    private final int orderlineNumber;

    private int binId;

    public Slab(int slabId, int schNum, int ordNum) {
        this.slabId = slabId;
        scheduleNumber = schNum;
        orderlineNumber = ordNum;
    }

    /**
     * @return the slabId
     */
    public int getId() {
        return slabId;
    }

    /**
     * @return the scheduleNumber
     */
    public int getScheduleNumber() {
        return scheduleNumber;
    }

    /**
     * @return the orderlineNumber
     */
    public int getOrderlineNumber() {
        return orderlineNumber;
    }

    /**
     * @return the binId
     */
    public int getBinId() {
        return binId;
    }

    /**
     * @param binId the binId to set
     */
    public void setBinId(int binId) {
        this.binId = binId;
    }
}
