package com.em.yms.optimization.testbed;

import static com.em.yms.optimization.testbed.Simulator.Algorithm.SCHEDULE_AND_ORDERLINE_ONLY;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Edward Ames
 */
public class IndoorYardMaterialSelection {

    private final Simulator simulator;
    private final int numRuns;

    private final Map<Integer, Integer> schedules;
    private final Map<Integer, Simulator.MultipleInSameOrderline> orderlineGroups;

    public IndoorYardMaterialSelection(int nRuns) {
        numRuns = nRuns;

        schedules = new HashMap<>();
        schedules.put(425, 200);
        schedules.put(426, 200);
        schedules.put(427, 200);

        orderlineGroups = new HashMap<>();
        Simulator.MultipleInSameOrderline orderline = new Simulator.MultipleInSameOrderline();
        orderline.orderline = 20;
        orderline.numInOrderline = 10;
        orderlineGroups.put(426, orderline);

        simulator = new Simulator();
    }

    public void run() {
        simulator.clear();
        simulator.setupSlabs(schedules, orderlineGroups);

        List<Integer> runSchedules = new ArrayList<>();
        runSchedules.add(425);
        Simulator.Algorithm algoType = SCHEDULE_AND_ORDERLINE_ONLY;
        Simulator.SimulationResults results = 
                simulator.runSim(numRuns, algoType, runSchedules);

        System.out.println(results);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length != 1 || "-h".equals(args[0]) || "--help".equals(args[0])) {
            System.out.println("IndoorYarMaterialSelection <numRun>");
            System.exit(0);
        }

        int numRun = Integer.parseInt(args[0]);
//        System.out.println("number of Runs = " + numRun);
        IndoorYardMaterialSelection instance =
                new IndoorYardMaterialSelection(numRun);
        instance.run();
    }
}
