package com.em.yms.optimization.testbed;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Edward Ames
 */
public class Bin {
    private final int binId, x, y, maxNumSlabs;
    private final List<Slab> slabs;

    public Bin(int id, int xx, int yy, int n) {
        binId = id;
        x = xx;
        y = yy;
        maxNumSlabs = n;
        slabs = new ArrayList<>();
    }

    public int addSlab(Slab slab) {
        if (slabs.size() >= maxNumSlabs) {
            return -1;
        } else {
            slabs.add(slab);
            return slabs.size();
        }
    }
    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @return the maxNumSlabs
     */
    public int getMaxNumSlabs() {
        return maxNumSlabs;
    }

    /**
     * @return the numSlabs
     */
    public int getNumSlabs() {
        return slabs.size();
    }

    /**
     * @return the binId
     */
    public int getBinId() {
        return binId;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Bin (id = ");
        sb.append(binId);
        sb.append(" x,y = ");
        sb.append(x);
        sb.append(",");
        sb.append(y);
        sb.append("  slabs = [");
        String foo = "";
        for (Slab slab : slabs) {
            sb.append(foo);
            sb.append(" ");
            sb.append(slab.getId());
            foo = ",";
        }
        sb.append("])");
        return sb.toString();
    }
}
