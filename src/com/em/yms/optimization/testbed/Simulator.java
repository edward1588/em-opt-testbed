package com.em.yms.optimization.testbed;

import com.em.yms.optimization.testbed.algorithm.MaterialSelector;
import com.em.yms.optimization.testbed.algorithm.MaterialSelector.RunData;
import com.em.yms.optimization.testbed.algorithm.simple.ScheduleAndOrderlineOnly;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author Edward Ames
 */
public class Simulator {
    private static final int NUM_CHARGING_BINS = 8;
    private static final int NUM_BINS_HORIZONTAL = 30;
    private static final int NUM_BINS_VERTICAL = 10;

    private static final int NUM_SLABS_IN_BIN = 10;
    private static final int DIST_BETWEEN_BINS = 10;

    static final int SLAB_ID_STARTING_ID = 10000;
    static final int BIN_ID_STARTING_ID  =  1000;

    private final Map<Integer, ChargingSkidBin> chargingSkid;
    private final Random rand;

    private final Inventory inventory;
    private final BinLayout binLayout;

    private int slabIdCounter = SLAB_ID_STARTING_ID;
    private int binIdCounter  =  BIN_ID_STARTING_ID;

    public Simulator() {
        chargingSkid = new HashMap<>();
        binLayout = new BinLayout();
        inventory = new Inventory();

        rand = new Random();
    }
    public SimulationResults runSim(int numRuns, Algorithm algoType, List<Integer> scheduleNums) {
        MaterialSelector algo;
        switch(algoType) {
            case SCHEDULE_AND_ORDERLINE_ONLY:
                algo = new ScheduleAndOrderlineOnly(inventory, binLayout);
                break;
            default:
                return new SimulationResults();
        }

        List<RunData> results = new ArrayList<>();

        for (int i = 0; i < numRuns; i++) {
            results.add(algo.simulate(scheduleNums));
        }

        return analyseResults(results);
    }

    private SimulationResults analyseResults(List<RunData> results) {
        SimulationResults answer = new SimulationResults();
        for (RunData data : results) {
            answer.n++;
            answer.avgDist += data.distance;
            if (data.moves != null) {
                answer.avgNumMoves += data.moves.size();
            }
        }
        answer.avgDist /= results.size();
        answer.avgNumMoves /= results.size();
        return answer;
    }

    public static class SimulationResults {
        double avgDist;
        double stdDev;
        double avgNumMoves;
        int n;
        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Number of runs = ");
            sb.append(n);
            sb.append(",  avg Distance = ");
            sb.append(avgDist);
            sb.append(",  avg Number of PMGs = ");
            sb.append(avgNumMoves);
            return sb.toString();
        }
    }
    public enum Algorithm{
        SCHEDULE_AND_ORDERLINE_ONLY
    }

    public void clear() {
        binLayout.clear();
        inventory.clear();
    }
    /**
     * Put slabs in the bins.
     *
     * @param schedules        Map<scheduleNum, numberInSchedule>
     * @param orderlineGroups  Map<scheduleNum, <orderline, numInSameOrderline>>
     */
    public void setupSlabs(Map<Integer, Integer> schedules,
            Map<Integer, MultipleInSameOrderline> orderlineGroups) {
        setupBins();

        int maxNumBins = NUM_BINS_HORIZONTAL * NUM_BINS_VERTICAL;

        int orderline, numInSchedule, binId;
        boolean useGroups;

        for (Integer scheduleNum : schedules.keySet()) {
            numInSchedule = schedules.get(scheduleNum);
            orderline = 1;
            useGroups = false;
            if (orderlineGroups.containsKey(scheduleNum)) {
                useGroups = true;
            }

            for (int i = 1; i <= numInSchedule; i++) {
                if (useGroups && i == orderlineGroups.get(scheduleNum).orderline) {
                    int numWithSameOrderline = orderlineGroups.get(scheduleNum).numInOrderline;
                    for (int j = 0; j < numWithSameOrderline; j++) {
                        Slab slab = new Slab(slabIdCounter++, scheduleNum, orderline);
                        inventory.addSlab(slab);
                        binId = rand.nextInt(maxNumBins) + BIN_ID_STARTING_ID;
                        while (binLayout.addSlabToBin(binId, slab) == -1) {
                            binId = rand.nextInt(maxNumBins);
                        }
                        slab.setBinId(binId);
                        i++;
                    }
                }

                Slab slab = new Slab(slabIdCounter++, scheduleNum, orderline);
                inventory.addSlab(slab);
                binId = rand.nextInt(maxNumBins) + BIN_ID_STARTING_ID;
                while (binLayout.addSlabToBin(binId, slab) == -1) {
                    binId = rand.nextInt(maxNumBins);
                }
                slab.setBinId(binId);

                orderline++;
            }
        }
    }

    public static class MultipleInSameOrderline {
        public int orderline;
        public int numInOrderline;
    }

    private void setupBins() {
        for (int x = 0; x < NUM_BINS_HORIZONTAL; x++) {
            for (int y = 0; y < NUM_BINS_VERTICAL; y++) {
                Bin bin =
                        new Bin(
                                binIdCounter++,
                                DIST_BETWEEN_BINS*x + 10,
                                DIST_BETWEEN_BINS*y + 40,
                                NUM_SLABS_IN_BIN);
                binLayout.addBin(bin);
            }
        }

        for (int x = 0; x < NUM_CHARGING_BINS; x++) {
            ChargingSkidBin bin =
                    new ChargingSkidBin(
                            x,
                            DIST_BETWEEN_BINS*x + 10,
                            20,
                            0);
            chargingSkid.put(x, bin);
        }
    }

    /**
     * @return the inventory
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * @return the binLayout
     */
    public BinLayout getBinLayout() {
        return binLayout;
    }
}
