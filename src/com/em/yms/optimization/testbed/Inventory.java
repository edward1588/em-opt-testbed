package com.em.yms.optimization.testbed;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Edward Ames
 */
public class Inventory {
    /**  Map<slabId, Slab>.  */
    private final Map<Integer, Slab> slabs;
    /**  Map<scheduleNum, List<slabId>>.  */
    private final Map<Integer, List<Integer>> slabsBySchedule;

    public Inventory() {
        slabs = new HashMap<>();
        slabsBySchedule = new HashMap<>();
    }

    public void addSlab(Slab aSlab) {
        slabs.put(aSlab.getId(), aSlab);

        List<Integer> slabsInSchedule = slabsBySchedule.get(aSlab.getScheduleNumber());
        if (slabsInSchedule == null) {
            slabsInSchedule = new ArrayList<>();
        }
        slabsInSchedule.add(aSlab.getId());
        slabsBySchedule.put(aSlab.getScheduleNumber(), slabsInSchedule);
    }

    public List<Integer> getSlabIdsInSchedule(int scheduleNum) {
        return slabsBySchedule.get(scheduleNum);
    }
    public List<Slab> getSlabsInSchedule(int scheduleNum) {
        List<Integer> scheduledIds = slabsBySchedule.get(scheduleNum);
        List<Slab> scheduledSlabs = new ArrayList<>();
        for (Integer slabId : scheduledIds) {
            scheduledSlabs.add(slabs.get(slabId));
        }
        return scheduledSlabs;
    }
    public Slab getSlab(int slabId) {
        return slabs.get(slabId);
    }
    public int getNumSlabs() {
        return slabs.size();
    }
    public void clear() {
        slabs.clear();
        slabsBySchedule.clear();
    }
}
