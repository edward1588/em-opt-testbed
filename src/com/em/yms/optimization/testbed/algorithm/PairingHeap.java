package com.em.yms.optimization.testbed.algorithm;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Edward Ames
 * @param <T>
 */
public class PairingHeap<T extends Comparable> {
    /**  Minimum value stored.  */
    private final T n;
    /**  Rest of stored values.  */
    private final List<PairingHeap> subheaps;

    /**
     * Constructor.
     *
     * @param val    Value to be stored.
     * @param heaps  Other values to be stored.
     */
    public PairingHeap(T val, List<PairingHeap> heaps) {
        n = val;
        subheaps = heaps;
    }
    /**
     * Get the minimum value in the heap without deleting the element.
     *
     * @return    Minimum value in the heap.
     */
    public T findMin() {
        return n;
    }
    /**
     * Combine two heaps into a third.  Merge a given heap with the heap calling
     * the function.
     *
     * @param heap2  Second heap.
     * @return       Combined heap with both initiating heap and supplied heap.
     */
    public PairingHeap merge(PairingHeap heap2) {
        if (heap2 == null)
            return this;
        if (n.compareTo(heap2.n) < 0) {
            subheaps.add(0, heap2);
            return this;
        } else {
            heap2.subheaps.add(0, this);
            return heap2;
        }
    }
    /**
     * Add a new value.  Gives a new heap.
     * 
     * @param x   New value to include.
     * @return    New heap with value included.
     */
    public PairingHeap insert(T x) {
        return merge(new PairingHeap(x, new ArrayList<>()));
    }
    /**
     * Get a new heap without the minimum value from initiating heap.
     *
     * @return   New heap without the current minimum.
     */
    public PairingHeap deleteMin() {
        return mergePairs(subheaps);
    }
    /**
     * Merges the subheaps in pairs (reason why datastructure has name)
     * from left to right and then merges the resulting list of heaps from right to left.
     *
     * @param x   List of subheaps to combine into a single heap.
     * @return    Combined heap including all subheaps.
     */
    private PairingHeap mergePairs(List<PairingHeap> x) {
        if (x == null || x.isEmpty()) {
            return null;
        } else if (x.size() == 1) {
            return x.get(0);
        } else {
            return x.get(0).merge(x.get(1)).merge(mergePairs(x.subList(2, x.size())));
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(n);
        sb.append(":");
        sb.append(subheaps.toString());
        sb.append(" ");
        return sb.toString();
    }
}
