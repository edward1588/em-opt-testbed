package com.em.yms.optimization.testbed.algorithm.simple;

import com.em.yms.optimization.testbed.Bin;
import com.em.yms.optimization.testbed.BinLayout;
import com.em.yms.optimization.testbed.Inventory;
import com.em.yms.optimization.testbed.Slab;
import com.em.yms.optimization.testbed.algorithm.MaterialSelector;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Edward Ames
 */
public class ScheduleAndOrderlineOnly implements MaterialSelector {

    private final Inventory inventory;
    private final BinLayout binLayout;

    public ScheduleAndOrderlineOnly(Inventory invent, BinLayout bins) {
        inventory = invent;
        binLayout = bins;
    }

    @Override
    public RunData simulate(List<Integer> schedules) {
        RunData results = new RunData();
        List<List<Integer>> moves = new ArrayList<>();
        List<Integer> plannedMove = new ArrayList<>();
        double currX, currY, nextX, nextY, dist;

        List<Slab> slabs = prepareSlabs(schedules);

        currX = 0;  currY = 0;  dist = 0;
        for (Slab slab : slabs) {
            if (plannedMove.size() >= 4) {
                moves.add(plannedMove);
                plannedMove = new ArrayList<>();
            }
            plannedMove.add(slab.getId());

            Bin bin = binLayout.getBin(slab.getBinId());
            nextX = bin.getX();  nextY = bin.getY();
            dist += Math.sqrt((nextX - currX) * (nextX - currX)
                    + (nextY - currY) * (nextY - currY));
            currX = nextX;  currY = nextY;
        }
        moves.add(plannedMove);
        results.distance = dist;
        results.moves = moves;

        return results;
    }
    List<Slab> prepareSlabs(List<Integer> schedules) {
        List<Slab> slabs = new ArrayList<>();
        for (Integer scheduleNum : schedules) {
            slabs.addAll(inventory.getSlabsInSchedule(scheduleNum));
        }
        Collections.sort(slabs, new SortSlab());
        return slabs;
    }

    private class SortSlab implements Comparator<Slab>
    {
        @Override
        public int compare(Slab a, Slab b)
        {
            int answer = a.getScheduleNumber() - b.getScheduleNumber();
            if (answer == 0) {
                answer = a.getOrderlineNumber() - b.getOrderlineNumber();
            }
            return answer;
        }
    }
}
