package com.em.yms.optimization.testbed.algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Edward Ames
 */
public class EdgeWeightedGraph {
    private final int V;                         // number of vertices
    private int E;                               // number of edges
    private final Map<Integer, List<Edge>> adj;  // adjacency lists
    public EdgeWeightedGraph(int V) {
        this.V = V;
        this.E = 0;
        adj = new HashMap<>();
        for (int v = 0; v < V; v++) {
            adj.put(v, new ArrayList<>());
        }
    }
    public int V() { return V; }
    public int E() { return E; }
    public void addEdge(Edge e) {
        int v = e.either(), w = e.other(v);
        adj.get(v).add(e);
        adj.get(w).add(e);
        E++;
    }
    public Iterable<Edge> adj(int v) {
        return adj.get(v);
    }
    public Edge getEdge0(int v) {
        return adj.get(v).get(0);
    }

    /**
     * Returns all edges in this edge-weighted graph.
     * To iterate over the edges in this edge-weighted graph, use foreach notation:
     * {@code for (Edge e : G.edges())}.
     *
     * @return all edges in this edge-weighted graph, as an iterable
     */
    public Iterable<Edge> edges() {
        Bag<Edge> list = new Bag<>();
        for (int v = 0; v < V; v++) {
            int selfLoops = 0;
            for (Edge e : adj(v)) {
                if (e.other(v) > v) {
                    list.add(e);
                }
                // add only one copy of each self loop (self loops will be consecutive)
                else if (e.other(v) == v) {
                    if (selfLoops % 2 == 0) list.add(e);
                    selfLoops++;
                }
            }
        }
        return list;
    }

    /**
     * Returns a string representation of the edge-weighted graph.
     * This method takes time proportional to <em>E</em> + <em>V</em>.
     *
     * @return the number of vertices <em>V</em>, followed by the number of edges <em>E</em>,
     *         followed by the <em>V</em> adjacency lists of edges
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(V).append(" ").append(E).append("\n");
        for (int v = 0; v < V; v++) {
            s.append(v).append(": ");
            for (Edge e : adj.get(v)) {
                s.append(e).append("  ");
            }
            s.append("\n");
        }
        return s.toString();
    }
}
