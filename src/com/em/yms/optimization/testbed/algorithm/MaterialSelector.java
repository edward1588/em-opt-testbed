package com.em.yms.optimization.testbed.algorithm;

import java.util.List;

/**
 *
 * @author Edward Ames
 */
public interface MaterialSelector {
    RunData simulate(List<Integer> schedules);
    public class RunData {
        /**  Total distance for all planned moves.  */
        public double distance;
        /**  List< PlannedMovementGroup List<SlabId>>.  */
        public List<List<Integer>> moves;
    }
}
