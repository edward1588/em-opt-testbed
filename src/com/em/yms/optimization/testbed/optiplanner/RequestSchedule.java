package com.em.yms.optimization.testbed.optiplanner;

import com.em.yms.optimization.testbed.Bin;
import java.util.Collection;
import java.util.List;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.score.Score;
import org.optaplanner.core.api.score.buildin.simple.SimpleScore;

/**
 *
 * @author Edward Ames
 */
@PlanningSolution
public class RequestSchedule implements Solution {
    private List<Bin> bins;
    private List<Path> plans;
    private SimpleScore score;

    @ProblemFactCollectionProperty
    public List<Bin> getBins() {
        return bins;
    }
    @PlanningEntityCollectionProperty
    public List<Path> getPlans() {
        return plans;
    }
    @PlanningScore
    @Override
    public SimpleScore getScore() {
        return score;
    }

    @Override
    public void setScore(Score s) {
        this.score = (SimpleScore) s;
    }

    @Override
    public Collection getProblemFacts() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
