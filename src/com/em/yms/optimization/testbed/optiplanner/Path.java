package com.em.yms.optimization.testbed.optiplanner;

import com.em.yms.optimization.testbed.Bin;
import java.util.ArrayList;
import java.util.List;
import org.optaplanner.core.api.domain.entity.PlanningEntity;

/**
 *
 * @author Edward Ames
 */
@PlanningEntity
public class Path {
    private double distance;
    public List<Bin> getBins() {
        return new ArrayList<>();
    }
}
