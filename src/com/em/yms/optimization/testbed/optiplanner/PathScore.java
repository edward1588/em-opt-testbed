package com.em.yms.optimization.testbed.optiplanner;

import org.optaplanner.core.api.score.buildin.simple.SimpleScore;
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator;

/**
 *
 * @author Edward Ames
 */
public class PathScore implements EasyScoreCalculator<RequestSchedule> {
    @Override
    public SimpleScore calculateScore(RequestSchedule solution) {
        int score = 0;
        return SimpleScore.valueOf(score);
    }
}
