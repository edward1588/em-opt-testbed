package com.em.yms.optimization.testbed.optiplanner;

import com.em.yms.optimization.testbed.BinLayout;
import com.em.yms.optimization.testbed.Inventory;
import com.em.yms.optimization.testbed.algorithm.MaterialSelector;
import java.util.ArrayList;
import java.util.List;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.score.director.ScoreDirectorFactoryConfig;
import org.optaplanner.core.config.solver.EnvironmentMode;
import org.optaplanner.core.config.solver.SolverConfig;

/**
 *
 * @author Edward Ames
 */
public class Optiplanner implements MaterialSelector {

    private final Inventory inventory;
    private final BinLayout binLayout;

    public Optiplanner(Inventory invent, BinLayout bins) {
        inventory = invent;
        binLayout = bins;
    }

    @Override
    public RunData simulate(List<Integer> schedules) {
        SolverConfig config = new SolverConfig();

        config.setSolutionClass(RequestSchedule.class);

        List<Class<?>> entities = new ArrayList<>();
        entities.add(Path.class);
        config.setEntityClassList(entities);

        ScoreDirectorFactoryConfig scoreConfig = new ScoreDirectorFactoryConfig();
        scoreConfig.setEasyScoreCalculatorClass(PathScore.class);
        config.setScoreDirectorFactoryConfig(scoreConfig);
        
        config.setEnvironmentMode(EnvironmentMode.FAST_ASSERT);

//        SolverFactory<RequestSchedule> factory = SolverFactory
//            .createFromXmlResource("configFile/MaterialSelectionIndoor.xml");
        SolverFactory<RequestSchedule> factory = SolverFactory.createEmpty();
        factory.getSolverConfig().inherit(config);

        Solver<RequestSchedule> solver = factory.buildSolver();

        // @PlanningSolution
        RequestSchedule problem = new RequestSchedule();
        RequestSchedule solution = solver.solve(problem);

        RunData results = new RunData();
        results.distance = solution.getScore().getScore();
        return results;
    }
}
