package com.em.yms.optimization.testbed;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Edward Ames
 */
public class BinLayout {
        /**  
     * Holds slabs (both scheduled and unscheduled) in each bin.
     * The key is binId (from database), and ArrayList of slabId's in bin.
     * Map<binId, List<SlabId>>.
     */
    private final Map<Integer, Bin> bins;

    public BinLayout() {
        bins = new HashMap<>();
    }
    public void addBin(Bin aBin) {
        bins.put(aBin.getBinId(), aBin);
    }
    public int addSlabToBin(int binId, Slab slab) {
        return bins.get(binId).addSlab(slab);
    }
    public Bin getBin(int binId) {
        return bins.get(binId);
    }
    public void clear() {
        bins.clear();
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Bin bin : bins.values()) {
            sb.append(bin);
            sb.append("\n");
        }
        return sb.toString();
    }
}
